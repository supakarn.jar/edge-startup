#!/bin/bash

if ! command -v curl &> /dev/null; then
    echo "installing curl"
    sudo apt install curl
else
    echo "curl found"
fi

if ! command -v wget &> /dev/null; then
    echo "install wget"
    sudo apt install wget
else
    echo "wget found"
fi
current_timezone=$(timedatectl | grep "Time zone" | awk '{print $3}')

# Function to reconfigure timezone
reconfigure_timezone() {
    sudo timedatectl set-timezone Asia/Taipei
    echo "Timezone reconfigured to Asia/Taipei."
}

# Main script
if [ "$current_timezone" != "Asia/Taipei" ]; then
    echo "Current timezone is $current_timezone. Reconfiguring..."
    reconfigure_timezone
else
    echo "Timezone is already set to Asia/Taipei."
fi


if ! command -v docker &>/dev/null; then
    echo "Initializing system"
    curl http://localhost/install.sh -o install.sh
    chmod +x install.sh
    ./install.sh
    if [ $? -ne 0 ]; then
        echo "Failed to initialize system"
        exit 1
    fi
    echo "System initialized"
fi

echo "All the system requirements meet, exiting..."
