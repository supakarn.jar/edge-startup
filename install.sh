#!/bin/bash
echo "Checking docker installation"
if [[ $(uname -i) == arm* ]];then
    ARCH=arm64
else
    ARCH=amd64
fi
DEB_FOLDER=deb
CONTAINERD_VERSION=1.6.21-1
DOCKER_CE_VERSION=24.0.2-1
DOCKER_CLI_VERSION=24.0.2-1
COMPOSE_VERSION=2.6.0

if command -v docker &> /dev/null; then
    echo "Docker installation found"
    docker --version
    echo "Skip docker installation"
else
    echo "Docker installation not found. Proceed to install docker"
    echo "Downloading required docker package"
    mkdir $DEB_FOLDER
    wget https://download.docker.com/linux/ubuntu/dists/bionic/pool/stable/${ARCH}/containerd.io_${CONTAINERD_VERSION}_${ARCH}.deb -P $DEB_FOLDER
    wget https://download.docker.com/linux/ubuntu/dists/bionic/pool/stable/${ARCH}/docker-ce_${DOCKER_CE_VERSION}~ubuntu.18.04~bionic_${ARCH}.deb -P $DEB_FOLDER
    wget https://download.docker.com/linux/ubuntu/dists/bionic/pool/stable/${ARCH}/docker-ce-cli_${DOCKER_CLI_VERSION}~ubuntu.18.04~bionic_${ARCH}.deb -P $DEB_FOLDER
    wget https://download.docker.com/linux/ubuntu/dists/bionic/pool/stable/${ARCH}/docker-compose-plugin_${COMPOSE_VERSION}~ubuntu-bionic_${ARCH}.deb -P $DEB_FOLDER
    sudo dpkg -i ${DEB_FOLDER}/containerd.io_${CONTAINERD_VERSION}_${ARCH}.deb \
            ${DEB_FOLDER}/docker-ce_${DOCKER_CE_VERSION}~ubuntu.18.04~bionic_${ARCH}.deb \
            ${DEB_FOLDER}/docker-ce-cli_${DOCKER_CLI_VERSION}~ubuntu.18.04~bionic_${ARCH}.deb \
            ${DEB_FOLDER}/docker-compose-plugin_${COMPOSE_VERSION}~ubuntu-bionic_${ARCH}.deb
    if [[ $? -eq 0 ]]; then
        echo "Docker installed successfully"
        rm -rf deb
    else
        echo "Error installing Docker"
        exit 1
    fi
fi

sudo mkdir -p /opt/app
if [[ $(docker ps --format '{{.Image}}' | grep collector | wc -l) -ne 3 ]]; then
    echo "Running crawler..."
    sudo mkdir -p /opt/app/crawler
    curl http://localhost/crawler/docker-compose.yml -o  /opt/app/crawler/docker-compose.yml ## change localhost to the http server ip
    sudo cd /opt/app/crawler
    sudo docker compose up -d
else
    echo "Crawler is already running"
fi
