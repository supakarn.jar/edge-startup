# Edge-startup

## Deploy environment
- 1 HTTP server (as cloud representative)
- 1 edge device

## STEP
1. **ON HTTP SERVER** install http server
```bash
# ubuntu
sudo apt install apache2

# centos
sudo apt install httpd
```

2. **ON HTTP SERVER** copy `install.sh` and `crawler` folder to `/var/www/html`
3.  **ON HTTP SERVER** start http server
```bash
# ubuntu
sudo systemctl start apache2

# centos
sudo systemctl start httpd
```

4. **ON EDGE DEVICE** copy and run `startup.sh`
```bash
./startup.sh
```